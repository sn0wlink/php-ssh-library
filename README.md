# SSH Library for PHP
A basic library to allow file transfers and checking to be done over SSH with SSH Keys.
The private key should be stored in /var/www/.ssh with '600' file permissions.

Don't run the client on a public facing server. It's only meant to grab files off public servers.

## Rules
- Clean and well formatted files is a MUST!
- Line width soft limit to 80 characters (I use a 80char rule)
- Use 4 spaces (NOT TABS)
- Use predefined templates if available
- Always use monospace fonts to display correctly
- Be excellent to each other.
