<?php

////////////////////////////////////////////////////////////////////////////////
//
// Freds SSH File Transfer
// Author: David Collins-Cubitt
// Client: Teal Press Ltd
// Date: Jan 2021
// Note: SSH Keys have to be setup
//
////////////////////////////////////////////////////////////////////////////////

// INSTALLATION - README //

// Place the private ssh keys into the /var/www/.ssh folder and set the file
// permissions to 600

// FUNCTIONS AVAILABLE //

// sshf_copy_to();
// sshf_copy_from();
// sshf_del();
// sshf_datemod();
// sshf_compare();

////////////////////////////////////////////////////////////////////////////////

// Copy to remote location
function sshf_copy_to($sshUser, $ipAddress, $From, $To, $chmod) {

    $AutoAcceptKeys = TRUE;  // Set to automatically accept server fingerprint

    // Setup Var
    $AutoAccept = '';
    $ParityCheckLocal  = '';
    $ParityCheckRemote = '';

    if ($AutoAcceptKeys) {
        $AutoAccept = '-o StrictHostKeyChecking=no';
    }
    
    // Start the file copy
    shell_exec("scp $AutoAccept $From $sshUser@$ipAddress:$To");

    // Setup Owner and group
    shell_exec("ssh $AutoAccept $sshUser@$ipAddress 'chown www-data:www-data $To'");

    // Set File Permissions
    shell_exec("ssh $AutoAccept $sshUser@$ipAddress 'chmod $chmod $To'");

    // Local file check parity MD5
    $MD5Local = shell_exec("md5sum $From");
    // Filter just the MD5 string
    $MD5Local = substr($MD5Local, 0, 32);

    // Remote file check parity MD5
    $MD5Remote = shell_exec("ssh $AutoAccept $sshUser@$ipAddress 'md5sum $To'");
    // Filter just the MD5 string
    $MD5Remote = substr($MD5Remote, 0, 32);

    // Check and return result
    if ($MD5Local == $MD5Remote) {
       return 'FILEOK';
    }
    // If fails, display error
    else {
        return 'FAILED';
    }   
}

// Copy from remote location
function sshf_copy_from($sshUser, $ipAddress, $From, $To) {

    $AutoAcceptKeys = TRUE;  // Set to automatically accept server fingerprint

    // Setup Var
    $AutoAccept = '';

    if ($AutoAcceptKeys) {
        $AutoAccept = '-o StrictHostKeyChecking=no';
    }
    
    // Start the file copy
    shell_exec("scp $AutoAccept $sshUser@$ipAddress:$From $To");

    // Set File Permissions
    shell_exec("chmod $To");

    // Local file check parity MD5
    $MD5Local = shell_exec("md5sum $To");
    // Filter just the MD5 string
    $MD5Local = substr($MD5Local, 0, 32);

    // Remote file check parity MD5
    $MD5Remote = shell_exec("ssh $AutoAccept $sshUser@$ipAddress 'md5sum $From'");
    // Filter just the MD5 string
    $MD5Remote = substr($MD5Remote, 0, 32);

    // Check and return result
    if ($MD5Local == $MD5Remote) {
       return 'FILEOK';
    }
    // If fails, display error
    else {
        return 'FAILED';
    }
}

// Deletes a remote file
function sshf_del($sshUser, $ipAddress, $Filename) {

    $AutoAcceptKeys = TRUE;  // Set to automatically accept server fingerprint

    // Setup Var
    $AutoAccept = '';

    if ($AutoAcceptKeys) {
        $AutoAccept = '-o StrictHostKeyChecking=no';
    }

    // Delete the remote file
    shell_exec("ssh $AutoAccept $sshUser@$ipAddress 'rm $Filename'");
}

// Check when file was last modified
function sshf_datemod($sshUser, $ipAddress, $Filename) {
    $AutoAcceptKeys = TRUE;  // Set to automatically accept server fingerprint

    // Setup Var
    $AutoAccept = '';

    if ($AutoAcceptKeys) {
        $AutoAccept = '-o StrictHostKeyChecking=no';
    }

    // Get Date modified stats
    $cmd = shell_exec("ssh $AutoAccept $sshUser@$ipAddress 'stat -c '%Y' $Filename'");
    return $cmd;
}

// Check sync with remote file
function sshf_compare($sshUser, $ipAddress, $Local, $Remote, $chmod) {

    $AutoAcceptKeys = TRUE;  // Set to automatically accept server fingerprint

    // Setup Var
    $AutoAccept = '';
    $ParityCheckLocal  = '';
    $ParityCheckRemote = '';

    if ($AutoAcceptKeys) {
        $AutoAccept = '-o StrictHostKeyChecking=no';
    }
    
    // Local file check parity MD5
    $MD5Local = shell_exec("md5sum $Local");
    // Filter just the MD5 string
    $MD5Local = substr($MD5Local, 0, 32);

    // Remote file check parity MD5
    $MD5Remote = shell_exec("ssh $AutoAccept $sshUser@$ipAddress 'md5sum $Remote'");
    // Filter just the MD5 string
    $MD5Remote = substr($MD5Remote, 0, 32);

    // Check and return result
    if ($MD5Local == $MD5Remote) {
       return 'CHECKOK';
    }
    // If fails, display error
    else {
        return 'CHECKERROR';
    }   
}

?>
